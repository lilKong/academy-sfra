'use strict';

/**
 * @namespace NewsLetterNew
 */

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');
var seekableiterator = require('dw/util/SeekableIterator')
var paymentMgr = require('dw/order/PaymentMgr');
var list = require('dw/util/List');

/**
 * Any customization on this endpoint, also requires update for Default-Start endpoint
 */
/**
 * Hello-Show : This endpoint is called when a shopper navigates to the hello page
 * @name Base/NewsLetterNew-Create
 * @function
 * @memberof NewsLetterNew
 * @param {middleware} - consentTracking.consent
 * @param {middleware} - cache.applyDefaultCache
 * @param {category} - non-sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */

server.get('Create', server.middleware.https, function (req, res, next) {

    var customObjectName = req.querystring.name;
    var customObjectSurname = req.querystring.surname;
    var customObjectEmail = req.querystring.email;

    try{

        var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
        var jsonResponse = {};

        if(customObjectName != null && customObjectSurname != null && customObjectEmail != null) {

            if(dw.system.Site.getCurrent().getCustomPreferenceValue("check_custom_object")) {
                transaction.wrap(function () {           
                    var customObject = customObjectMgr.createCustomObject(dw.system.Site.getCurrent().getCustomPreferenceValue("newsletter_subscription"), currentTimeStamp);
                    customObject.custom.timestamp = currentTimeStamp;
                    customObject.custom.firstname = customObjectName;
                    customObject.custom.lastname = customObjectSurname;
                    customObject.custom.email = customObjectEmail;
    
                    var success = "Custom Object created successfully!";
                    jsonResponse.success = success;
                    jsonResponse.customObjectFirstName = customObject.custom.firstname;
                    jsonResponse.customObjectLastName = customObject.custom.lastname;
                    jsonResponse.customObjectEmail = customObject.custom.email;
                    jsonResponse.customObjectTimestamp = customObject.custom.timestamp;
            });
            } else {
                var error = "Error! Couldn't create the Custom Object. One or more parameter(s) are invalid, please check your values";
                jsonResponse.error = error;
            }
            
        } else {
            var error = "Error! Couldn't create the Custom Object. One or more parameter(s) are invalid, please check your values";
            jsonResponse.error = error;
        }

        res.json(jsonResponse);
        next();

    } catch (e) {
        res.json(e);
    } 
});

server.get('Read', server.middleware.https, function (req, res, next) {
    try{
        var jsonResponse = {};
        jsonResponse.item = [];

        seekableiterator = customObjectMgr.getAllCustomObjects(dw.system.Site.getCurrent().getCustomPreferenceValue("newsletter_subscription"));
        while(seekableiterator.hasNext()) {    
            var item = seekableiterator.next();
            var itemJson = {};
            itemJson.timestamp = item.custom.timestamp;
            itemJson.firstname = item.custom.firstname;
            itemJson.lastname = item.custom.lastname;
            itemJson.email = item.custom.email;
            jsonResponse.item.push(itemJson);
        }

        jsonResponse.count = seekableiterator.count;

        res.json(jsonResponse);
        next();

    } catch (e) {
        res.json(e);
    }
});

server.get('Delete', server.middleware.https, function (req, res, next) {

    var jsonResponse = {};
    var customObjectTimestamp = req.querystring.timestamp;

    if(customObjectTimestamp != null || customObjectTimestamp != '') {
        try {
            var customObject = customObjectMgr.getCustomObject('newslettersubscription', customObjectTimestamp);

            transaction.wrap(function () {
                customObjectMgr.remove(customObject);

                var status='Custom Object [newslettersubscription] '+customObjectTimestamp+' removed';
                jsonResponse.status=status;
            }); 

        } catch (e) {
            var status='!ERROR! Custom Object [newslettersubscription] '+customObjectTimestamp+' not removed';
            jsonResponse.status=status;
        }
    }
    
    res.json(jsonResponse)
    next();
    
});

server.get('Payment', server.middleware.https, function (req, res, next) {
    try {
        var jsonResponse = {};
        jsonResponse.item = [];
        var list = paymentMgr.getActivePaymentMethods();

        for(var i=0; i<list.length; i++) {
            var item = list.get(0);
            var itemJson = {};
            itemJson.id = item.ID;
            itemJson.uuid = item.UUID;
            itemJson.name = item.name;
            jsonResponse.item.push(itemJson);
        }

        res.json(jsonResponse);
        next();
    } catch (e) {

    }
});

server.get('ErrorNotFound', function (req, res, next) {
    res.setStatusCode(404);
    res.render('error/notFound');
    next();
});

module.exports = server.exports();
