'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
// Use the following for CSRF protection: add middleware in routes and hidden files
var CSRFProtection = require('*/cartridge/scripts/middleware/csrf');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {
        // var actionUrl = dw.web.URLUtils.url('Newsletter-Handler');
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: dw.web.URLUtils.url('Newsletter-Subscribe'),
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.post(
    'Subscribe',
    server.middleware.https,
    function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var newsletterForm = server.forms.getForm('newsletter');
        var email = newsletterForm.email.htmlValue;
        var fname = newsletterForm.fname.htmlValue;
        var lname = newsletterForm.lname.htmlValue;

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');
        var reqObject = {
            properties: {
                email: email,
                firstname: fname,
                lastname: lname
            }
        };

        var responseObj = service.call(reqObject);

        res.json({
            email: email,
            firstName: fname,
            lastName: lname,
            response: responseObj.ok ? responseObj.msg : "There was an error: " + responseObj.msg,
            success: responseObj.ok,
            redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()

        });
        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = dw.web.URLUtils.url('Newsletter-Show');

        // Perform any server-side validation before this point, and invalidate form accordingly
        if (newsletterForm.valid) {
            // Send back a success status, and a redirect to another route
            res.render('/newsletter/newslettersuccess', {
                continueUrl: continueUrl,
                newsletterForm: newsletterForm
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.render('/newsletter/newslettererror', {
                errorMsg: dw.web.Resource.msg('error.crossfieldvalidation', 'newsletter', null),
                continueUrl: continueUrl
            });
        }

        next();
    }
);

server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);


module.exports = server.exports();