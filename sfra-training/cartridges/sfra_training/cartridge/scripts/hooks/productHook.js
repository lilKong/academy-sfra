
function productDetails(product) {

    var HookMgr = require('dw/system/HookMgr');
    var prod = {};

    prod.id = product.getID();
    prod.name = product.getName();
    prod.desc = product.getShortDescription();

    HookMgr.callHook('sfra-training.createCustomProd', 'createProd', prod);

    return prod;
}

exports.productDetails = productDetails;
