
var Status = require('dw/system/Status');

function reportCustomProducts() {
    var Logger = require('dw/system/Logger');
    var customLogger = Logger.getLogger('LoggerTest', 'warn');
    var CustomObjectMgr = require('dw/object/CustomObjectMgr');

    var args = arguments[0];
    var allObjs = CustomObjectMgr.getAllCustomObjects(args.customObjectType);
    var objList = allObjs.asList();

    var products = [];

    for(var i = 0; i< objList.length; i++) {
        products.push('Product ID: '+ objList[i].custom.id + ', Product Name: ' + objList[i].custom.name + ', Produt Description: ' + objList[i].custom.short_desc + '\n');
    };

    customLogger.warn('All instances: {0}', products.toString());
    return new Status(Status.OK);
}

exports.reportCustomProducts = reportCustomProducts;