'use strict';

var formValidation = require('base/components/formValidation');

module.exports = {
    submitNewsletter: function () {
        $('form.newsletter-form').on('submit', function (e) {
            //console.log("prova");
            var $form = $(this);
            e.preventDefault();
            var url = $form.attr('data-url');
            //console.log(url);
            $form.spinner().start();
            $('form.newsletter-form').trigger('newsletter:submit', e);
            $.ajax({
                url: url,
                type: 'post',
                dataType: 'json',
                data: $form.serialize(),
                success: function (data) {
                    //console.log(data);
                    $form.spinner().stop();
                    if (!data.success) {
                        $('.subscription-feedback').text(data.response);
                        $('.subscription-feedback').addClass('failure');
                        $('.newsletter-result').removeClass('hidden');
                        // formValidation($form, data);
                    }
                    else {
                        //window.location.href = data.redirectUrl;
                        $('.subscription-feedback').text(data.response);
                        $('.subscription-feedback').addClass('success');
                        $('.newsletter-result').removeClass('hidden');
                        //console.log(data);
                    }
                },
                error: function (err) {
                    if (err.responseJSON.redirectUrl) {
                        window.location.href = err.responseJSON.redirectUrl;
                    }
                    $form.spinner().stop();
                }
            });
            return false;
        });
    }
};